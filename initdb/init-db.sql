
CREATE TYPE sexe AS ENUM ('FEMME', 'HOMME', 'AUTRE');

CREATE TABLE ville
(
    id        VARCHAR UNIQUE PRIMARY KEY,
    nom       VARCHAR NOT NULL,
    longitude NUMERIC NOT NULL,
    latitude  NUMERIC NOT NULL,
    UNIQUE (nom, latitude)
);


CREATE TABLE utilisateur
(
    id       VARCHAR UNIQUE PRIMARY KEY,
    prenom   VARCHAR                 NOT NULL,
    nom      VARCHAR                 NOT NULL,
    age      VARCHAR                 NOT NULL,
    ville_id VARCHAR                 NOT NULL REFERENCES ville (id),
    sexe     sexe NOT NULL
);

COMMENT ON COLUMN utilisateur.prenom is 'Est le prenom de l utilisateur';
COMMENT ON COLUMN utilisateur.nom is 'Est le nom de naissance de l utilisateur';
COMMENT ON COLUMN utilisateur.age is 'Est l age de l utilisateur';
COMMENT ON COLUMN utilisateur.ville_id is 'Est la clé primaire qui fait référence à la ville où l utilisateur habite';
COMMENT ON COLUMN utilisateur.sexe is 'Est le sexe de l utilisateur de type sexe';

INSERT INTO ville (id, nom, longitude, latitude)
VALUES ('c4e5fcc2-ab73-11ed-afa1-0242ac120002', 'Paris', 2.333333, 48.86);

INSERT INTO utilisateur (id, prenom, nom, age, ville_id, sexe)
VALUES ('f43692ad-9f11-4ce2-8d21-078e79ea21a3', 'Loïc', 'cam', 28, 'c4e5fcc2-ab73-11ed-afa1-0242ac120002', 'HOMME');